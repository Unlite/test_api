<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;
use common\models\Author;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model \common\models\Book*/

$this->title = 'Books';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="col-xs-4">
		<?= ListView::widget([
			'dataProvider' => new \yii\data\ActiveDataProvider([
				'query' => Author::find()
			]),
			'itemView' => '/author/_item',
			'emptyText' => "",
			'summary' => '',
			'layout' => '{items}'
		]);
		?>
    </div>
    <div class="col-xs-8">
		<?= DetailView::widget([
			'model' => $model,
			'attributes' => [
				'id',
				'author.name',
				'title',
			],
		]) ?>
    </div>
</div>
