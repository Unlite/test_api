<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;
use common\models\Author;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\BookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Books';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="col-xs-4">
		<?= ListView::widget([
			'dataProvider' => new \yii\data\ActiveDataProvider([
				'query' => Author::find()
			]),
			'itemView' => '/author/_item',
			'emptyText' => "",
			'summary' => '',
			'layout' => '{items}'
		]);
		?>
    </div>
    <div class="col-xs-8">
		<?= GridView::widget([
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'columns' => [
				'title',
				'author.name',
				[
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}'
                ],
			],
		]); ?>
    </div>
</div>
