<?php

namespace frontend\modules\v1\controllers;

use yii\rest\ActiveController;

class BookController extends ActiveController
{
	public $modelClass = 'common\models\Book';

	/**
	 * {@inheritdoc}
	 */
	public function actions()
	{
		$actions = parent::actions();

		$actions['index']['dataFilter'] = [
			'class' => 'yii\data\ActiveDataFilter',
			'searchModel' => 'backend\models\search\BookSearch'
		];

		return $actions;
	}

}
